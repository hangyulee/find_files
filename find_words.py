#!/usr/bin/env python3
# coding: utf-8
import os, glob, fnmatch



import itertools

# import argparse
import time
import os
import multiprocessing
import time
# path = input("location to find")
# target = input("target to find : ")
#path = path.replace(r'\', '\\')

how_many_files = 0

with open("words.txt", 'r', encoding='UTF8') as f:
    items = f.readlines()
path_pattern = items[0].split(",")
path = path_pattern[0]
patterns = path_pattern[1:]
target = items[1:]
# the difference between name of files and categories
print("\nPath = {} \ntarget = {} \n".format(path,target))



def worker(filepath):
        for pattern in patterns:
            if filepath.lower().endswith(pattern.lower().strip()):
                temp = filepath.split("/")

                #print(temp[len(temp)-1])
                #exit(0)
                filename = temp[len(temp)-1]
                            #return filename
                try:
                    with open(filepath,'r', encoding='UTF8', errors='ignore') as f:

                        out = f.read()
                        for word in target:
                           if word.lower() in out.lower():
                                print(filepath)
                                return None


                           # print(word.lower(), out.lower())
                           # if out.lower().find(word.lower()):
                           #     print(filepath)
                           #     return None

                except:
                    pass
def find_files(directory):

    global how_many_files
    print("START PROCESS, DIR NAME = {}\n\n".format(directory))
    with multiprocessing.Pool(32) as pool:  # pool of 48 processes
        walk = os.walk(directory)

        fn_gen = itertools.chain.from_iterable((os.path.join(root, basefile) for basefile in files) for root, dirs, files in walk)
        print("Generator Created")
        results_of_work = pool.map(worker, fn_gen)
        print("Found = {}".format(len(results_of_work)))


if __name__=="__main__":
    start = time.time()

    find_files(path.strip())

    end = time.time()
    #print("How many files looked up = {}".format(how_many_files))
    print("How long it takes = {}".format(end - start))

    input()


